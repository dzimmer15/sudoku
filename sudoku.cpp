
#include "sudoku.h"

SudokuCell::SudokuCell(int value)
{
    _value = value;
}

int SudokuCell::get_value()
{
    return _value;
}

std::vector<int> SudokuCell::get_pencil()
{
    return _pencil;
}

void SudokuCell::update_value(int value)
{
    _value = value;
}


void SudokuCell::update_pencillist(int value)
{
    // loop through all values in pencil list to find the pencil value. This may
    // this may need to be optimized for the future generic board so many more
    // values may be penciled in.
    for (auto &&x : _pencil) 
    {
        if (x == value)
        {

        }
    }
    _pencil.push_back(value);
}



Sudoku::Sudoku()
{
    
}

void Sudoku::test()
{
    std::cout << "test" << std::endl;
}


// this needs to be changed to a stream to pipe to a proper ui
void Sudoku::printBoard()
{
    std::cout << std::endl;
    for (int x = 0; x < 9; x++)
    {
        for (int y = 0; y < 9; y++)
        {
            std::cout << _grid[x][y].get_value() << " ";
        }
        std::cout << std::endl;
    }
}

void Sudoku::updateCell(int value, int x, int y, bool pencil)
{
    if (pencil) // checking if it is a pencil value has to be done at some point
                // and I think it's best to do it as low as possible to keep the
                // code as clean as possible.
    {
        _grid[y][x].update_pencillist(value);
    }
    else
    {
        _grid[y][x].update_value(value);
    }
}


int Sudoku::run()
{
    std::cout << "run" << std::endl;

    while (true)
    {
        int x, y, value;
        std::cout << "x ";
        std::cin >> x;
        std::cout << "y ";
        std::cin >> y;
        std::cout << "value ";
        std::cin >> value;

        updateCell(value, x, y);

        printBoard();
    }
}
