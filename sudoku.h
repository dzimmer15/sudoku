#include "vector"
#include "array"
#include "iostream"

#ifndef VECTOR_H
#define VECTOR_H



class SudokuCell
{
public:
    // initalizes the cell, default empty
    SudokuCell(int value = 0);

    // returns the value in this cell
    int get_value();       

    // returns a vector of values that the player creates to aid them in
    // finging the true value.
    std::vector<int> get_pencil();

    // Updates cell value
    void update_value(int value);

    // Adds value to pencil list or removes value from pencil list if value
    // already exists in the list.
    void update_pencillist(int value);


private:

    // the primary value of the cell
    // 0 is empty 1-9 is the default
    int _value;                     

    std::vector<int> _pencil;       // possible values written by the player to
                                    //     aid them in finding the true value.

    std::vector<void (**)()> _updateList;   // Event handler
};

class Sudoku
{
public:
    Sudoku();

    // runs the game
    int run(); 

    /* temporary test method used to demo game elements. 
    This should be removed when the grid is abstracted and the printBoard() 
    function is turned into a proper stream. */
    void test();


    //  prints the playing grid with written values. Does not include grid lines.
    
    void printBoard();

    //  updates the cell at x, y with value. For penciled numbers set pencil=true
    void updateCell(int value, int x, int y, bool pencil = false);

private:
    //the iconic 9x9 grid
    // using std array for stablity and ease of use. I want to make this 
    //expandable to games on larger boards and with unique group shapes. 
    //This needs to be templated in the near future.
    std::array<std::array<SudokuCell, 9>, 9> _grid;

};

#endif